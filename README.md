# Sanushi detector 

## Preliminaries

In determining how to model the task, the following thoughts came to mind. 

On one hand, one can consider 'sanushi' as being a new type of object that somehow jointly embodies the characteristics of 'sandwich' and 'sushi.' With this understanding, I would be tempted to treat both sandwiches and sushi as separate concepts and learn individual models for each against other background concepts. Then, in predicting sanushi I would select the images that score highly on both concepts. Unfortunately, I have only modest computational resources to hand (I own an old laptop with no GPU) so it was not feasible to acquire more data to learn these concepts from a wider set of 'background' concepts. I do, however, believe this is a sensible approach that is less sensitive to the problems of the next. 

On the other hand, from a foundational standpoint it is even challenging to formulate a bespoke likelihood function for the task. A simple likelihood-based approach might be to view 'sanushi' as those images that are hard to classify as either sandwich or sushi based on a classifier learnt on the training data. In other words, it may not be unreasonable to consider low confidence (or high-entropy) predictions to be sanushi. This perspective is not without problems since a large number of other confounding aspects may be responsible for the low predictive confidence (e.g. variation in lighting, composition, background, other elements, etc). I think, therefore, that sanushi selection based on high predictive entropy proffers unreasonable expectations on the explanation for low predictive confidence, since a great many reasons may induce a classifier to have low confidence besides a 'sanushi' concept. 

The proposed approach is explained in the next section.

## Approach 

The proposed approach is to investigate 'sanushi' using kernel embedding and statistical testing methods called [maximum mean discrepancy](http://www.jmlr.org/papers/volume13/gretton12a/gretton12a.pdf) (MMD). This approach is very powerful family of algorithms for performing statistical tests between classes, and I use only a small portion of it in this. MMD requires computing the Gram matrix of the data. For this, I used a classic RBF kernel, and used the [median heuristic](https://www.semanticscholar.org/paper/Appearance-based-Object-Recognition-using-SVMs%3A-I-Caputo-Sim/05914202a81210a5ad2f1b7b293179c1dd50e91b) for selecting its bandwidth parameter. I use a process commonly found in the MMD literature here as a heuristic for ranking the data. MMD requires us to first quantify how similar the data are to sandwich features first and then to sushi features. The difference between these are then calculated. Intuitively, the more negative this number is the more similar an image is to sushi, and similarly the more positive this number the more similar it is to sandwich. Importantly, when the difference is close to 0, it indicates the image is close to both sandwich and sushi features. 

The full pipeline is outined below. 

- Acquire image representations using the pre-trained networks enumerated below. 
	- alexnet 
	- vgg11 
	- inception v2
	- mobilenet v2
	- ensemble of the above
- Construct feature embedding visualisations of the data to explore their structure. 
- Evaluate the quality of feature representations quantitatively by learning and evaluating predictive models on the class label. 
- Settle on a final feature representation (from the set mentioned above) based on the qualitative and quantitative analysis from the previous two steps.
- Use an MMD approach to select the instances that are 'close' both sandwich and sushi images.
- Select the top 10 images that were deemed to be close to sandwich and sushi based on MMD. 

The result of this pipeline is stored in the `./sanushi` directory. `./sanushi/selected.txt` enumerates the files that are most sanushi-like, `./sanushi/sanushi.pdf` is the collection of the top 9 images based on the criteria outlined above, and the remaining JPG images are copies of the raw images. 
 
## Running the code 

The simplest way to produce the outputs given in this repository are to run the following command:  

```bash 
make requirements  # To install all python modules
make all           # To run the whole pipeline 
```

This will extract features to a directory called `./build`. First, the data are stratified into five distinct folds, an these are found within the `build`. These data are augmented with 5 random transformations (see the `randomising_transformer` function in `models.py`). Next, features are extracted from the augmented dataset using `alexnet`, `vgg`, `inception` and `mobilenet` pre-trained networks. The representations are dumped to `./build/{pretrained-network-name}`, for example `alexnet` features are dumped to `./build/alexnet/*`. 

These features are used for three purposes: 

1. Exploratory data analysis. In `dataset-analysis.ipynb` broad statistics from the dataset are presented. Additionally feature embeddings are produced. These are saved to `./figures/umap-{pretrained-network-name}.pdf`. 
2. Classification models are learnt on the task, classifying between `sandwich` and `sushi` according to the path of the original images. See `classification-analysis.ipynb` for the classification metrics (all suggestive that the "ensemble" representation is best). Some analytic figures are dumped in `./figures`. 
3. The features are used with a maximum mean discrepancy method for sanushi selection.

Each of these tasks are executed in the correct sequence by the Makefile. 

## Analysis 

I split the analysis into quantitative and qualitative. The quantitative analysis is the result of the classification sub-tasks that fed into the feature selection, and can be seen in greater detail in `classification-analysis.ipynb`. Qualitative analysis is reserved analytics performed on the global dataset and determination of the quality of the sanushi selection. 

### Quantitative

Three classification models were investigated: Gaussian mixture model (density models learnt on each class, and predictions given by Bayes' rule), logistic regression (SGD), and logistic regression with principal component analysis (PCA) for feature selection (PCA-SGD). The test results (achieved by 5-fold cross validation) are shown in the table below. They show that the task of classifying between sandwich and sushi is reasonably well captured by the models (accuracy of approx. 91%). In all cases, the "ensemble" features are shown to out-perform the other feature representations, and this led me to use ensemble features for the MMD task. The best-performing individual feature are from mobilenet. 

|           |    GMM   |  PCA-SGD |    SGD   |
|----------:|:--------:|:--------:|:--------:|
|   alexnet | 0.799910 | 0.857460 | 0.853019 |
|  ensemble | 0.891503 | 0.916407 | 0.914185 |
| inception | 0.858963 | 0.881312 | 0.877046 |
| mobilenet | 0.877102 | 0.900278 | 0.901540 |
|     vgg11 | 0.834849 | 0.880546 | 0.883056 |

Cross validation was performed over a broad grid in all cases. The figure below shows the cross validation scores on each of the feature representations. The parameter under investigation here is the L2 regulariser on the weight norm of a logistic regression model. As the L2 parameter grows large the the classifier classifyies at random. The classifier ranking in cross validation scores seems to match the ranking on the test set given in the table above.  This provides strong evidence for using the ensemble features for the MMD pipeline. 

<img src="./figures/xval-acc-sgd.png" alt="Crossvalidation scores" width="500"/>

Cross validation on the GMM and PCA-SGD model can be found in the `./figures/` directory. 

The table below shows the confusion matrix of the (augmented) dataset using the ensemble representation. We can see here that there is a much higher false-positive rater for sushi (approx 11%) than there is for sandwich (approx 6%). 

|                 | Predicted sandwich | Predicted sushi |  Sum  |
|----------------:|:------------------:|:---------------:|:-----:|
| Actual sandwich |        1884        |       126       |  2010 |
|    Actual sushi |         219        |       1791      |  2010 |
|             Sum |        2103        |       1917      |  4020 |


### Qualitative

The figure below shows the feature visualisation of the (augmented) data using [uniform manifold approximation and projection](https://github.com/lmcinnes/umap) (UMAP) embeddings. In general, the classes are well-separated with the ensemble representation. This is a positive discovery since UMAP projects global data structure, and the embeddings are not guided with knowledge of the class labels. Thus, this figure suggests that sushi and sandwich are embedded into (loosely) distinct regions of feature space. Additionally, we can see that there is a lot more sushi in the domain of sandwiches than sandwiches in the sushi domain. This visualisation therefore serves as an explanation for the skewed error rates that were shown in the confusion matrix above. 

<img src="./figures/umap-ensemble.png" alt="Crossvalidation scores" width="500"/>

Using the MMD procedure, the top 9 selected sanushi images are shown here: 

<img src="./sanushi/sanushi.png" alt="Sanushi selection" width="500"/>

The analysis that I outline here is mostly subjective since I have found it challenging to define or formulate a 'sanushi' measure. From my perspective, it seems that the best example of 'sanushi' selected by the heuristic is the image (top row, right), which depicts a sandwich that appears to be rolled into a sushi roll. The figure in (middle row, middle) is labelled as sandwich, but to me it appears to be more of a salad, with croutons being the main hook to sandwiches. Additionally, the salad appears to contain rashers, which may have produced meat-like textures that are commonly seen with sushi. 

The image on (bottom row, left) appears to be a very poor selection for sanushi: it clearly depicts a bread concept. Found within this image are some possibly distracting elements (there appears to be rice above, and some ingredients that may be contained in sushi on the top left). These elements may have caused the model to detected both a sandwich and sushi within the image. The image is quite small however (280x210) which may serve as an explanation to this surprise. 

I would have had trouble classifying the images (middle row, left) and (middle row, right). With knowledge of their label I understand why they have been classified like that, but -- particularly with the left image -- it is unclear which is the main element of the composition. 

To contrast this analysis, I show the 25 most typical examples of sandwiches/sushi from MMD. The examples shown here look like very typical version of the food types. 

<img src="./sanushi/sandwich_or_sushi.png" alt="Sanushi selection" width="500"/>

All of these images are clear examples of sandwich or sushi. I think these serve to explain why the clear sandwich (top row, middle) of the sanushi grid is classified as sanushi: the colours and textures are reminiscent to some of the sushi in the grid here. However, the image also depicts clear bread-like textures, which (I believe) is why it was selected as sanushi. 

## Conclusions

This is an interesting task. I was not certain how to formalise the problem, nor how to then optimise for it.  Although I believe the approach taken is reasonable, I also feel that such a task is exploratory and requires some degree of human interaction to find a suitable set of sanushi images. In particular, It would be useful to pre-process the images and crop or mask out non-sandwich and non-sushi elements. The proposed procedure is shown to pick out some examples of sanushi (particularly top row, right of 3x3 grid), but some of the selected elements are also disappointing. 

In general, the method seems to select images that are not clearly belonging to either class, and it does not appear to prioritise what might belong to both classes. Thus, I think that learning bespoke sandwich and sushi concepts from a broader dataset would be a promising approach. It should prioritise images that jointly embody the characteristics of both sandwiches and sushi, and provide a better overall ranking of the challenge. 

## Improvements 

The following should be considered to improve the pipeline

- Distance model for MMD. 
- Larger dataset. 
- Optimise (feature) models. 
- Learn separate sandwich and sushi concepts from the larger dataset. 
- Cropping images on the food.
- Investigate bespoke loss functions. 

# Numeric operations
import numpy as np
import torch

# For loading dataset
from PIL import Image

# For evaluation
from sklearn.model_selection import train_test_split

# For decorator
from functools import wraps
import json

from logger import get_logger

__all__ = [
    "figure_loader", "train_test_val_split", "get_logger",
    "validate_feat_name", "allowed_feature_names",
    "RNGInit", "NumpyEncoder",
]

logger = get_logger(__name__)

allowed_feature_names = {
    "alexnet",
    "mobilenet",
    "vgg11",
    "inception",
    "ensemble",
}


def figure_loader(fname):
    """
    Load a figure from filee

    :param fname: str
        The path to a file
    :return: PIL.Image
    """
    
    return Image.open(fname)


def train_test_val_split(inds, test_size, val_size):
    """
    Define the indices for train, validation and test splits.

    :param inds: int or 1d array-like
        A range object defining the train/validation/test indices.
    :param test_size: float
        The proportion of the full dataset set for the train split.
    :param val_size: float
        The proportion of the train split set for the validation size.
    :return: tuple, length 3
        The elements of the tuple correspond to the train, validation and test inds respectively.
    """
    
    # Split train&validation from test
    if isinstance(inds, int):
        inds = np.arange(inds)
    train_val_inds, test_inds = train_test_split(inds[:, None], test_size=test_size)
    
    # split train and validation from train&validation
    train_inds, val_inds = train_test_split(train_val_inds, test_size=val_size)
    
    return train_inds.ravel(), val_inds.ravel(), test_inds.ravel()


def validate_feat_name(feat_name):
    """
    Verify that the feature name provided is valid.
    
    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11"}. str
        The input name
    :return: str
        Returns the feature name if it has been validated. Otherwise, ValueError is raised.
    """
    
    feat_name = feat_name.lower()
    
    logger.info(f"Verifying {feat_name} in {allowed_feature_names}")
    if feat_name not in allowed_feature_names:
        msg = f"Unsupported feature representation: {feat_name} not in {allowed_feature_names}"
        logger.error(msg)
        raise ValueError(msg)
    
    return feat_name


class RNGInit(object):
    """
    This decorator is used to standar
    """
    
    def __init__(self, seed=42):
        """

        :param seed:
        :return:
        """
        self.seed = seed
    
    def __call__(self, func):
        """

        :param func:
        :return:
        """
        
        @wraps(func)
        def wrapped_func(*args, **kwargs):
            # Set the numpy seed
            np.random.seed(self.seed)
            
            # Set the torch/cuda seed
            torch.manual_seed(self.seed)
            torch.backends.cudnn.benchmark = False
            torch.backends.cudnn.deterministic = True
            return func(*args, **kwargs)
        
        return wrapped_func


class NumpyEncoder(json.JSONEncoder):
    """
    This simple JSONEncoder class allows numpy ndarrays to be serialised to file.
    This is useful for dumping the cross-validation data resulting from a grid search
    to a JSON record.
    """
    
    def default(self, obj):
        """
        This function allows numpy ndarrays to be serialised. Call with:
        
        ```python
        import numpy as np
        import json
        
        d = dict(one=np.linspace(0, 1, 7))
        print(json.dumps(d))  # raises error
        print(json.dumps(d, cls=NumpyEncoder))
        ```

        :param obj: The object to be serialised
        :return: Encoder object
        """
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

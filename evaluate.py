# For directory structure
from os.path import exists, join
from os import makedirs

# Numeric operation
import numpy as np

# For dumping results
import json

# For parameter/model selection
from sklearn.model_selection import GridSearchCV, PredefinedSplit
from sklearn.metrics import classification
from sklearn.externals import joblib

from utils import get_logger, validate_feat_name, RNGInit, NumpyEncoder, allowed_feature_names
from models import sgd_clf_with_grid, pcasgd_clf_with_grid, gmm_clf_with_grid

logger = get_logger(__name__)


def classification_metrics(yy, y_hat):
    """
    This function returns several accuracy metrics based on ground truth and predicted labels.
    
    :param yy: 1d array-like.
    :param y_hat: 1d array-like.
    :return: dictionary of evaluated performance.
    """
    
    # Global metrics
    res = dict(
        accuracy=classification.accuracy_score(yy, y_hat),
        f1_macro=classification.f1_score(yy, y_hat, average="macro"),
        f1_micro=classification.f1_score(yy, y_hat, average="micro"),
        f1_weighted=classification.f1_score(yy, y_hat, average="weighted"),
        precision_macro=classification.precision_score(yy, y_hat, average="macro"),
        precision_micro=classification.precision_score(yy, y_hat, average="micro"),
        precision_weighted=classification.precision_score(yy, y_hat, average="weighted"),
        recall_macro=classification.recall_score(yy, y_hat, average="macro"),
        recall_micro=classification.recall_score(yy, y_hat, average="micro"),
        recall_weighted=classification.recall_score(yy, y_hat, average="weighted"),
        confusion_matrix=classification.confusion_matrix(yy, y_hat),
        per_class=dict()
    )
    
    # Also compute metrics on a per-class basis.
    for yi in np.unique(yy):
        yy_i = yy == yi
        y_hat_i = y_hat == yi
        res["per_class"][f"y={yi}"] = dict(
            accuracy=classification.accuracy_score(yy_i, y_hat_i),
            f1=classification.f1_score(yy_i, y_hat_i),
            precision=classification.precision_score(yy_i, y_hat_i),
            recall=classification.recall_score(yy_i, y_hat_i),
            confusion_matrix=classification.confusion_matrix(yy_i, y_hat_i),
        )
    
    return res


def load_folded_data(feat_root, feat_name, fold_id, n_folds):
    """
    
    :param feat_root: str
        This parameter defines the output directory into which the train, validation and test folds
        has been saved.
    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11"}.
    :param fold_id: int
        The fold identifier that is to be evaluated.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :return: tuple, length=3
        The first element is a tuple of training data (x_train, y_train, y_str_train, p_train).
        The second element is a pre-defined cross validation object across the non-test set.
        The third element is a tuple of testing data (x_test, y_test, y_str_test, p_test).
    """
    
    # The test data is defined by fold_id.
    if fold_id is not None:
        test = joblib.load(join(
            feat_root, feat_name, f"fold_{fold_id}.pkl"
        ))
    else:
        test = None
    
    # Build the training set from the *other* folds
    x_train, y_train, y_str_train, p_train, inds = [], [], [], [], []
    for fold_id_ in set(range(n_folds)) - {fold_id}:
        xx, yy, yy_str, pp = joblib.load(join(
            feat_root, feat_name, f"fold_{fold_id_}.pkl"
        ))
        
        x_train.extend(xx)
        y_train.extend(yy)
        y_str_train.extend(yy_str)
        p_train.extend(pp)
        inds.extend([fold_id_] * len(xx))
    
    cv = PredefinedSplit(inds)
    train = np.asarray(x_train), np.asarray(y_train), y_str_train, p_train
    
    return train, cv, test


def load_data(feat_root, feat_name, fold_id, n_folds):
    """
    
    :param feat_root: str
        This parameter defines the output directory into which the train, validation and test folds
        has been saved.
    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11", "ensemble"}.
    :param fold_id: int
        The fold identifier that is to be evaluated.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :return: tuple, length=3
        The first element is a tuple of training data (x_train, y_train, y_str_train, p_train).
        The second element is a pre-defined cross validation object across the non-test set.
        The third element is a tuple of testing data (x_test, y_test, y_str_test, p_test).
    """
    
    feat_name = validate_feat_name(feat_name)
    
    if feat_name == "ensemble":
        # When the feature representation is requested, all others are loaded. Features are
        # concatenated across the rows.
        sorted_names = sorted(set(allowed_feature_names) - {"ensemble"})
        train, cv, test = load_folded_data(
            feat_root=feat_root,
            feat_name=sorted_names[0],
            fold_id=fold_id,
            n_folds=n_folds,
        )
        
        for feat_name_ in sorted_names[1:]:
            train_, _, test_ = load_folded_data(
                feat_root=feat_root,
                feat_name=feat_name_,
                fold_id=fold_id,
                n_folds=n_folds,
            )
            
            train = ((np.concatenate((train[0], train_[0]), axis=1),) + train[1:])
            if test is not None:
                test = ((np.concatenate((test[0], test_[0]), axis=1),) + test[1:])
        
        return train, cv, test
    
    return load_folded_data(
        feat_root=feat_root,
        feat_name=feat_name,
        fold_id=fold_id,
        n_folds=n_folds,
    )


def evaluate_fold(model_name, feat_name, fold_id, n_folds, feat_root, results_root):
    """
    This function learns and evaluates one fold of the train/validation/test data.

    :param model_name: str
        The name of the classification model to be evaluated. Currently only "sgd" is specified.
    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11", "ensemble"}.
    :param fold_id: int
        The fold identifier that is to be evaluated.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :param feat_root: str
        This parameter defines the output directory into which the train, validation and test folds
        has been saved.
    :param results_root: str
        This specifies the directory into which the results and model files are to be saved.
    :return: dict
        A dictionary of prediction results on the train, validation and test splits.
    """
    
    # Build up the directory structure for the result and model files
    res_dir = join(results_root, feat_name, model_name)
    if not exists(res_dir):
        logger.info(f"Creating results directory {res_dir}")
        makedirs(res_dir)
    
    # Determine whether it is necessary to re-compute results
    result_fname = join(res_dir, f"fold_{fold_id}.json")
    model_fname = join(res_dir, f"fold_{fold_id}.sklearn")
    pred_fname = join(res_dir, f"fold_{fold_id}.pred")
    
    if exists(result_fname) and exists(model_fname) and exists(pred_fname):
        logger.info(f"Results file {result_fname} already exists. Loading from file.")
        with open(result_fname, "r") as fil:
            return json.load(fil)
    
    # Load the training, validation and test data
    (x_train, y_train, _, _), cv, (x_test, y_test, y_str_test, path_test) = load_data(
        feat_root=feat_root, feat_name=feat_name, fold_id=fold_id, n_folds=n_folds
    )
    
    # Build the model and fit it
    logger.info(f"Learning {model_name} from {feat_name} (fold {fold_id}); {x_train.shape}")
    model, grid = dict(
        sgd=sgd_clf_with_grid,
        pcasgd=pcasgd_clf_with_grid,
        gmm=gmm_clf_with_grid,
    )[model_name]()
    clf = GridSearchCV(
        estimator=model,
        param_grid=grid,
        verbose=10,
        cv=cv,
    )
    clf.fit(x_train, y_train)
    logger.info(f"Learning complete: ")
    
    # Build up train, validation and test results
    results = dict(
        train=classification_metrics(y_train, clf.predict(x_train)),
        test=classification_metrics(y_test, clf.predict(x_test)),
        xval=clf.cv_results_,
        xval_grid=grid, fold_id=fold_id,
    )
    
    # Dump the model
    logger.info(f"Dumping model to {model_fname}")
    joblib.dump(clf, model_fname)
    
    clf = joblib.load(model_fname)
    
    # Dump the predictions
    logger.info(f"Dumping predictions to {pred_fname}")
    if model_name == "gmm":
        xx = x_test.copy()
        for c in clf.best_estimator_[:-1]:
            xx = c.transform(xx)
        probs = clf.best_estimator_[-1].score_samples(xx)
    else:
        probs = clf.predict_proba(x_test)
    joblib.dump((probs, y_test, y_str_test, path_test,), pred_fname)
    
    # Dump the results
    logger.info(f"Dumping results to {result_fname}")
    with open(result_fname, "w") as fil:
        json.dump(
            obj=results,
            fp=fil,
            sort_keys=True,
            indent=2,
            cls=NumpyEncoder,
        )
    
    return results


def evaluate_model(model_name, feat_root, feat_name, n_folds, results_root="results"):
    """
    This function will evaluate the feature representation and model across the `n_folds` folds

    :param model_name: str
        The name of the classification model to be evaluated. Currently only "sgd" is specified.
    :param feat_root: str
        This parameter defines the output directory into which the train, validation and test folds
        has been saved.
    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11", "ensemble"}.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :param results_root: str
        This specifies the directory into which the results and model files are to be saved.
    :return:
    """
    
    feat_name = validate_feat_name(feat_name)
    
    results = []
    # Iterate over n_folds, and evaluate each fold.
    for fold_id in range(0, n_folds, 1):
        logger.info(f"Evaluating fold {fold_id} from {feat_name} features with {model_name}.")
        results.append(evaluate_fold(
            model_name=model_name,
            feat_name=feat_name,
            fold_id=fold_id,
            n_folds=n_folds,
            results_root=results_root,
            feat_root=feat_root,
        ))
    
    # Aggregated results are dumped to file
    with open(join(results_root, feat_name, f"{model_name}-results.json"), "w") as fil:
        json.dump(obj=results, fp=fil, indent=2, sort_keys=True, cls=NumpyEncoder)
    
    return results


@RNGInit(seed=42)
def main(feat_name, model_name):
    evaluate_model(
        feat_root="build",
        feat_name=feat_name,
        model_name=model_name,
        n_folds=5,
    )


if __name__ == "__main__":
    import sys
    
    main('ensemble', 'gmm')
    main(sys.argv[1], sys.argv[2])

.PHONY: clean data requirements features alexnet inception vgg11 mobilenet ensemble analysis all

PROJECT_NAME = sanushi

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PYTHON_INTERPRETER = python3

MAKE = /usr/bin/make

DATA_URL := http://research.us-east-1.s3.amazonaws.com/public/sushi_or_sandwich_photos.zip
DATA_DIR := sushi_or_sandwich
ZIP_FILE := sushi_or_sandwich_photos.zip


## Download and extrat data, and clean up additional files
data:
	if [ ! -d $(DATA_DIR) ] ; then wget $(DATA_URL); unzip $(ZIP_FILE); rm $(ZIP_FILE); rm -rf __MACOSX; fi

## Clear all downloaded data (not generated features/results)
clean:
	if [ -f $(ZIP_FILE) ] ; then rm $(ZIP_FILE) ; fi
	if [ -d $(DATA_DIR) ] ; then rm -r $(DATA_DIR) ; fi
	if [ -d __MACOSX ] ; then rm -r __MACOSX ; fi

## Instaell dependencies of the project
requirements:
	pip install -r requirements.txt

## Defines a function that extracts and evaluates features for a named task
define feat_extract_and_evaluate
#	$(PYTHON_INTERPRETER) features.py $(1)
#	$(PYTHON_INTERPRETER) evaluate.py $(1) sgd
#	$(PYTHON_INTERPRETER) evaluate.py $(1) pcasgd
	$(PYTHON_INTERPRETER) evaluate.py $(1) gmm
endef

## Run alexnet on the dataset
alexnet: data
	$(call feat_extract_and_evaluate, alexnet)

## Run inception on the dataset
inception: data
	$(call feat_extract_and_evaluate, inception)

## Run mobilenet on the dataset
mobilenet: data
	$(call feat_extract_and_evaluate, mobilenet)

## Run vgg11 on the dataset
vgg11: data
	$(call feat_extract_and_evaluate, vgg11)

## Run the ensemble model
ensemble: alexnet inception mobilenet vgg11
	$(call feat_extract_and_evaluate, ensemble)

## Extract and evaluate all features
features: alexnet inception mobilenet vgg11 ensemble

## Produce a
all: features
	$(PYTHON_INTERPRETER) select_sanushi.py

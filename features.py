# For directory structure
from os.path import exists, join
from os import makedirs

from sklearn.externals import joblib
from sklearn.model_selection import StratifiedKFold

# Numeric operations
from torchvision import datasets
import torch
import numpy as np

# For visual progress
from tqdm import trange

from utils import figure_loader, get_logger, validate_feat_name, RNGInit

from models import (
    load_mobilenet_features, load_inception_features,
    load_alexnet_features, load_vgg11_features,
)

logger = get_logger(__name__)


def generate_features(feat_name, feat_func, n_folds, out_dir):
    """
    Build the feature representation from pre-trained networks.

    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11", "ensemble"}.
    :param feat_func: callable
        This parameter is a callable function that is expected to return a pytorch tensor. Generally,
        `feat_func` will a PyTorch module.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :param out_dir: str
        This parameter defines the output directory into which the train, validation and test folds
        will be saved.
    :return: None
        This function does not return anything, but rather saves data.
    """
    
    # Ensure the required directory structure exists
    out_dir_ = join(out_dir, feat_name)
    if not exists(out_dir_):
        logger.info(f"Creating feature directory {out_dir_}")
        makedirs(out_dir_)
    
    # Avoid computation if features already calculated
    if all(exists(join(out_dir_, f"fold_{fid}.pkl")) for fid in range(n_folds)):
        logger.info(f"{feat_name} features already calculated, skipping.")
        return
    
    # Ensure that the model is in evaluation mode
    feat_func.eval()

    if torch.cuda.is_available():
        feat_func = feat_func.cuda()
    
    # This convenience function translates between numpy/pytorch for the desired representation
    def make_representation(xx, batch_size=100):
        zes = []
        for _ in trange(int(np.ceil(xx.shape[0] / batch_size))):
            zz, xx = xx[:batch_size], xx[batch_size:]  # quick way to achieve minibatches
            zz = torch.from_numpy(zz).float()  # numpy to torch tensor
            if torch.cuda.is_available():
                zz = zz.cuda()
            zz = feat_func(zz).detach()  # feature evaluation
            zz = zz.numpy().reshape(zz.shape[0], -1)  # torch to numpy + flatten
            zes.append(zz)
        return np.concatenate(zes, axis=0)
    
    # Repeat n_folds times
    for fold_id in range(n_folds):
        # Only recompute features if they don't exist
        fname = join(out_dir_, f"fold_{fold_id}.pkl")
        if exists(fname):
            logger.info(
                f"Features for {feat_name} on fold {fold_id} already calculated, skipping."
            )
            continue
        
        logger.info(f"Loading train/val/test data for fold {fold_id}")
        xx, yy, yy_str, pp = joblib.load(join(
            out_dir, f"fold_{fold_id}.pkl"
        ))
        
        logger.info(f"Building {feat_name} features on fold {fold_id}")
        xx_features = make_representation(xx)
        
        logger.info(f"Dumping {feat_name} fold {fold_id}")
        joblib.dump((xx_features, yy, yy_str, pp), fname)


def generate_folds(dataset_name, transforms, n_aug, n_folds, out_dir):
    """
    
    :param dataset_name: str
        This the name of the directory
    :param transforms: torchvision.transforms.Compose
        This parameter is a composed list of (potentially stochastic) transformations that are applied
        to the raw data.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :param n_aug: int
        This parameter defines the number of augmentation iterations that are performed.
    :param out_dir: str
        This parameter defines the output directory into which the train, validation and test folds
        will be saved.
    :return:
    """
    
    if not exists(out_dir):
        logger.info(f"Making output directory {out_dir}.")
        makedirs(out_dir)
    
    if all(exists(join(out_dir, f"fold_{fid}.pkl")) for fid in range(n_folds)):
        logger.info(f"Folds already calculated. Skipping re-computation.")
        return
    
    dataset_folder = datasets.DatasetFolder(
        dataset_name,
        loader=figure_loader,
        extensions=(".jpg",),
    )
    
    # Load all images for quick caching
    imgs = [
        (figure_loader(path), path, label) for path, label in dataset_folder.samples
    ]
    
    # A convenience function that does data augmentation and transformation
    def build_split_data(inds, desc):
        data, labels, label_str, path = [], [], [], []
        for aug_id in range(n_aug):
            logger.info(f"\tPreparing augmentation {aug_id}/{n_aug} for {desc}")
            for ind in inds:
                xi, pi, yi = imgs[ind]
                data.append(transforms(xi)[None, ...].detach().numpy().astype(float))
                label_str.append(["sandwich", "sushi"][yi])
                labels.append(yi)
                path.append(pi)
        data = np.concatenate(data, axis=0)
        labels = np.asarray(labels)
        path = np.asarray(path)
        return data, labels, label_str, path
    
    # Run over the folds
    xx = np.arange(len(imgs))[:, None]
    yy = np.asarray([img[2] for img in imgs])
    cv = StratifiedKFold(n_splits=n_folds)
    for fold_id, (_, test_inds) in enumerate(cv.split(xx, yy)):
        logger.info(f"Generating fold {fold_id}/{n_folds} for {dataset_folder.root}")
        xx, yy, yy_str, pp = build_split_data(test_inds, desc=f"Fold {fold_id}")
        
        logger.info(f"Dumping data")
        joblib.dump((xx, yy, yy_str, pp), join(out_dir, f"fold_{fold_id}.pkl"))


def generate_dataset(dataset_name, feat_name, n_folds, n_aug, out_dir="build"):
    """
    This function wraps the feature and dataset generation process for the `sushi_or_sandwich` dataset.

    :param dataset_name: str
        The name of the dataset under consideration
    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11", "ensemble"}.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :param n_aug: int
        This parameter defines the number of augmentation iterations that are performed.
    :param out_dir: str
        This defines the directory into which the data will be saved
    :return: None
        This function does not return anything, but rather saves the computed data to file.
    """
    
    feat_name = validate_feat_name(feat_name)
    logger.info(f"Generating {feat_name} features")
    
    # Create model and transformation objects
    feat_func, transforms = dict(
        inception=load_inception_features,
        mobilenet=load_mobilenet_features,
        alexnet=load_alexnet_features,
        vgg11=load_vgg11_features,
    )[feat_name]()
    
    # Make the pre-defined transformations
    generate_folds(
        dataset_name=dataset_name,
        transforms=transforms,
        n_aug=n_aug,
        n_folds=n_folds,
        out_dir=out_dir,
    )
    
    # Generate features from these transformations
    generate_features(
        feat_func=feat_func,
        feat_name=feat_name,
        n_folds=n_folds,
        out_dir=out_dir,
    )
    
    logger.info(f"{feat_name} features generated")


@RNGInit(seed=42)
def main(feat_name):
    generate_dataset(
        dataset_name="sushi_or_sandwich",
        feat_name=feat_name,
        n_folds=5,
        n_aug=5,
    )


if __name__ == "__main__":
    import sys
    
    main(sys.argv[1])

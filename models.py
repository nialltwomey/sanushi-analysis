import numpy as np

from scipy.special import logsumexp

from sklearn.base import ClassifierMixin, BaseEstimator
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from sklearn.mixture import GaussianMixture
from sklearn.decomposition import PCA
from sklearn.random_projection import GaussianRandomProjection

from torchvision import transforms, models

__all__ = [
    "randomising_transformer", "reshaping_transformer",
    "load_alexnet_features", "load_inception_features", "load_mobilenet_features", "load_vgg11_features",
    "sgd_clf_with_grid", "gmm_clf_with_grid", "pcasgd_clf_with_grid",
]


def randomising_transformer():
    """
    This function defines a transformer that randomises several aspects of the input image. Importantly,
    the transformer standardises the output image shape.

    :return: torchvision.transforms.Compose
        The composed transformer.
    """
    
    transformations = transforms.Compose([
        transforms.RandomHorizontalFlip(),
        transforms.RandomRotation(20),
        transforms.Resize(256),
        transforms.RandomCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225],
        )
    ])
    
    return transformations


def reshaping_transformer():
    """
    This function defines a transformer that centre-crops images to the standardised output image shape.

    :return: torchvision.transforms.Compose
        The composed transformer.
    """
    
    transformations = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225],
        )
    ])
    
    return transformations


def load_inception_features():
    """
    Loads the feature representation of the inception model.

    :return: tuple, length=2
        The feature model and the composed transformer.
    """
    
    transformations = randomising_transformer()
    inception = models.inception_v3(pretrained=True)
    return inception, transformations


def load_alexnet_features():
    """
    Loads the feature representation of the alexnet model.

    :return: tuple, length=2
        The feature model and the composed transformer.
    """
    
    transformations = randomising_transformer()
    alexnet = models.alexnet(pretrained=True)
    return alexnet, transformations


def load_vgg11_features():
    """
    Loads the feature representation of the vgg11 model.

    :return: tuple, length=2
        The feature model and the composed transformer.
    """
    
    transformations = randomising_transformer()
    vgg11 = models.vgg11(pretrained=True)
    return vgg11, transformations


def load_mobilenet_features():
    """
    Loads the feature representation of the mobilenet model.

    :return: tuple, length=2
        The feature model and the composed transformer.
    """
    
    transformations = randomising_transformer()
    mobilenet = models.mobilenet_v2(pretrained=True)
    return mobilenet, transformations


def sgd_clf_with_grid():
    """
    This function defines a simple SGD classification model and a grid search over the model parameters.

    :return: tuple, length=2
        An sklearn base classifier model, the grid search over hyperparameters.
    """
    
    grid = dict(
        clf__alpha=np.power(
            10.0, np.linspace(-10, 5, 31)
        ).tolist()
    )
    
    model = Pipeline([
        ("scale", StandardScaler()),
        ("clf", SGDClassifier(
            loss="log",
            penalty="l2",
            max_iter=250,
            tol=1e-9,
            verbose=0
        ))
    ])
    
    return model, grid


def pcasgd_clf_with_grid():
    """
    This function defines a simple SGD classification model preceded by PCA and a grid search over the model parameters.

    :return: tuple, length=2
        An sklearn base classifier model, the grid search over hyperparameters.
    """
    
    grid = dict(
        pca__n_components=[0.75, 0.8, 0.85, 0.9, 0.95, 0.99],
        clf__alpha=np.power(
            10.0, np.arange(-2, 2, 0.5),
        ).tolist()
    )
    
    model = Pipeline([
        ("scale", StandardScaler()),
        ("pca", PCA(0.9)),
        ("clf", SGDClassifier(
            loss="log",
            penalty="l2",
            max_iter=250,
            tol=1e-9,
            verbose=0
        ))
    ])
    
    return model, grid


class GMMClf(ClassifierMixin, BaseEstimator):
    """
    This is a classification model. It learns a Gaussian mixture model for each class.
    It works simply by slicing the training data on each class, and learning a separate
    GMM on each slice. In prediction, the log likelihood of the data is used.
    """
    
    def __init__(self, n_components=1):
        """
        :param n_components: int
            This specifies the number of components for the individual GMMMs
        """
        
        self.n_components = n_components
        self.n_classes = None
        self.models = None
    
    def fit(self, X, y):
        """
        Fit n_classes GMM models, one for each class.
        
        :param X: array-like, shape (n_samples, n_features)
        :param y: numpy array, shape (n_samples,)
        :return: self
        """
        
        unique_labels = np.unique(y)
        self.n_classes = unique_labels.shape[0]
        
        self.models = []
        for ii, yi in enumerate(unique_labels):
            model = GaussianMixture(
                n_components=self.n_components
            )
            model.fit(X[y == yi])
            self.models.append(model)
        
        return self
    
    def score_samples(self, X):
        """
        Evaluate the likelihood of the
        
        :param X: array-like, shape (n_samples, n_features)
        :return: numpy array, shape (n_samples, n_classes)
        """
        
        score_samples = np.concatenate(
            [model.score_samples(X)[:, None] for model in self.models], axis=1
        )
        
        return score_samples
    
    def predict(self, X):
        """
        Predict the class membership of the data X.
        
        :param X: array-like, shape (n_samples, n_features)
        :return: numpy array, shape (n_samples, n_classes)
        """
        
        return self.score_samples(X).argmax(axis=1)
    
    def predict_log_proba(self, X):
        """
        Predict the log probability of the data. The (i,j)-th component of the returned
        array is the probability that instance i belongs to the j-th class.
        
        :param X: array-like, shape (n_samples, n_features)
        :return: numpy array, shape (n_samples, n_classes)
        """
        
        scores = self.score_samples(X)
        scores -= logsumexp(scores, axis=1, keepdims=True)
        return scores
    
    def predict_proba(self, X):
        """
        Predict the probability of class membership of the data. The (i,j)-th component
        of the returned array is the probability that instance i belongs to the j-th class.
        
        :param X: array-like, shape (n_samples, n_features)
        :return: numpy array, shape (n_samples, n_classes)
        """
        
        return np.exp(self.predict_log_proba(X))
    
    def score(self, X, y, sample_weight=None):
        """
        Predict the log likelihood of the data.
        
        :param X: array-like, shape (n_samples, n_features)
        :param y:numpy array, shape (n_samples,)
        :param sample_weight: numpy array, shape (n_samples,) or None
        :return: float
        """
        
        scores = self.score_samples(X)
        return scores[np.arange(X.shape[0]), y].mean()


def gmm_clf_with_grid():
    """
    This function defines a simple SGD classification model and a grid search over the model parameters.

    :return: tuple, length=2
        An sklearn base classifier model, the grid search over hyperparameters.
    """
    
    grid = dict(
        gmm__n_components=[16],
        pca__n_components=[0.9]
    )
    
    model = Pipeline([
        ("scale", StandardScaler()),
        ("pca", PCA()),
        ("gmm", GMMClf()),
    ])
    
    return model, grid

from os.path import join

import numpy as np
import pandas as pd

from os.path import split, exists
from os import makedirs
from shutil import copyfile

from PIL import Image

import matplotlib.pyplot as pl

from sklearn.metrics import pairwise_distances

from evaluate import load_data

from utils import get_logger, figure_loader

logger = get_logger(__name__)


def tile_from_path(paths, n=5, size=(100, 100), ax=None):
    """
    This funtion takes a list of
    
    :param paths:
    :param n:
    :param size:
    :param ax:
    :return:
    """
    tile = np.zeros((size[0] * n, size[1] * n, 3), dtype=int)
    path_iter = iter(paths)
    ri = np.arange(size[0])
    ci = np.arange(size[1])
    for ii in range(0, size[0] * n, size[0]):
        for jj in range(0, size[1] * n, size[0]):
            try:
                img = figure_loader(next(path_iter))
                img = img.resize(size, Image.ANTIALIAS)
                tile[np.c_[ri + ii], ci + jj] = np.asarray(img)
            except StopIteration:
                pass
    if ax is None:
        fig, ax = pl.subplots(1, 1, figsize=(n * 5, n * 5))
    pl.sca(ax)
    pl.imshow(tile)
    pl.xticks([])
    pl.yticks([])
    pl.tight_layout()


def select_sanushi(feat_name="ensemble", n_folds=5, n_sanushi=10, out_dir="sanushi"):
    """
    This function takes an maximum mean discrepancy approach to take a feature representation
    produce a selection of n_sanushi
    
    :param feat_name: str
        The name of the feature representation presented to the classifier. Currently supported
        representations are defined in the set {"alexnet", "inception", "mobilenet", "vgg11", "ensemble"}.
    :param n_folds: int
        This parameter defines the number of folds that will be evaluated.
    :param n_sanushi: int
        This parameter defines the number of images to select as sanushi candidates.
    :param out_dir: str
        This defines the output path into which the selected sanushi images will be saved.
    :return:
    """
    
    logger.info(f"Loading {feat_name} features for sanushi selection procedure")
    xx, _, yy, path = load_data("build", feat_name, None, n_folds)[0]
    path = np.asarray(path)
    yy = np.asarray(yy)
    
    logger.info(f"Constructing data frame for convenience")
    df = pd.DataFrame(np.c_[xx, yy[:, None], path[:, None]])
    df = df.groupby(df.columns[-1]).first()
    xx, yy = df.values[:, :-1].astype(float), df.values[:, -1]
    
    path = df.index.values
    
    logger.info(f"Computing pairwise similarity between classes")
    D = pairwise_distances(xx, xx)
    D /= np.percentile(D.ravel(), 50)
    D = np.exp(-D)
    d_neg = D[:, yy == "sandwich"].sum(axis=1)
    d_pos = D[:, yy == "sushi"].sum(axis=1)
    d_diff = d_neg - d_pos  # pos-to-neg
    
    logger.info(f"Computing the between-class similarity metric")
    d_diff = np.abs(d_neg - d_pos - np.mean(d_diff))
    inds = np.argsort(d_diff)
    
    if not exists(out_dir):
        logger.info(f"Creating output directory {out_dir}")
        makedirs(out_dir)
    
    logger.info(f"Visualising typical sandwich/sushi")
    tile_from_path(path[inds[::-1]], n=5)
    pl.savefig(join(out_dir, "sandwich_or_sushi.png"))
    
    tile_from_path(path[inds], n=3)
    pl.savefig(join(out_dir, "sanushi.png"))
    pl.savefig(join(out_dir, "sanushi.pdf"))
    
    fname = join(out_dir, "selected.txt")
    logger.info(f"Saving info to {fname}")
    with open(fname, "w") as fil:
        for ind in inds[:n_sanushi]:
            pind = path[ind]
            fil.write(pind + "\n")
            copyfile(pind, join("sanushi", split(pind)[-1]))


if __name__ == "__main__":
    select_sanushi()

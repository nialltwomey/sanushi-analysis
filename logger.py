# Adapted from:
#   https://gist.github.com/nguyenkims/e92df0f8bd49973f0c94bddf36ed7fd0

import logging
import sys

__all__ = [
    "get_logger"
]

FORMATTER = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
LOG_FILE = "logging.log"


def get_console_handler():
    """A function that defines how logs will be printed to screen"""
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def get_file_handler():
    """A function that defines how logs will be serialised to file"""
    file_handler = logging.FileHandler(LOG_FILE, mode="w")
    file_handler.setFormatter(FORMATTER)
    return file_handler


def get_logger(logger_name):
    logger = logging.getLogger(logger_name)

    logger.setLevel(logging.DEBUG)  # better to have too much log than not enough

    logger.addHandler(get_console_handler())
    logger.addHandler(get_file_handler())

    # with this pattern, it"s rarely necessary to propagate the error up to parent
    logger.propagate = False

    return logger
